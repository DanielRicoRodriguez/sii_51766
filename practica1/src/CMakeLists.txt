INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

SET(CLIENTE_SRCS 
	MundoCliente.cpp 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)

SET(SERVIDOR_SRCS 
	MundoServidor.cpp 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)

ADD_EXECUTABLE(cliente cliente.cpp ${CLIENTE_SRCS})
ADD_EXECUTABLE(servidor servidor.cpp ${SERVIDOR_SRCS})

ADD_EXECUTABLE(bot bot.cpp)
ADD_EXECUTABLE(logger logger.cpp)


TARGET_LINK_LIBRARIES(cliente pthread glut GL GLU)
TARGET_LINK_LIBRARIES(servidor pthread glut GL GLU)
