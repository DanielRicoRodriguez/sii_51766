#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define TAM_BUFFER 100

int main()
{
	int fd, x=1;

	mkfifo("/tmp/FifoLogger",0777);
	fd = open("/tmp/FifoLogger",O_RDONLY);

	printf("\n");
	while(x)
	{
		char buffer [TAM_BUFFER];
		read(fd,buffer,sizeof(buffer));
		
		if(buffer[0]=='F')
		{
			printf("%s",buffer);
			x=0;
		}
		else	printf("%s",buffer);
	}
	close(fd);
	unlink("/tmp/FifoLogger");
	return 0;

}
